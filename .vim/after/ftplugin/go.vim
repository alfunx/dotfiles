setlocal tabstop=4
setlocal shiftwidth=4
setlocal noexpandtab

setlocal makeprg=go\ build\ -o\ target
